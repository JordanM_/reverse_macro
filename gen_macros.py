
letters = [chr(i) for i in range(ord('a'),ord('z')+1)]

if __name__ == "__main__":
	functions = []
	begin = 1
	end = 10
	for i in range(begin, end):
		arg_list = str(letters[:i])[1:-1].replace('\'','')
		#print(arg_list,  arg_list[::-1], i)
		print("#define FOO{0}({1}) {2}".format(i, arg_list, arg_list[::-1]))
		functions.append("FOO{0}".format(i))
		
		
	print("#define GET_MACRO({0},NAME,...) NAME".format(str(['_' + str(s) for s in list(range(begin, end))])[1:-1].replace('\'','')))
	print("#define FOO(...) GET_MACRO(__VA_ARGS__, {0})(__VA_ARGS__)".format(str(functions[::-1])[1:-1:].replace('\'','')))

